### Installing Node
Using NVM
- `curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh`\
- `cat install.sh | bash`\
- close terminal and open terminal\
- check nvm installed `command -v nvm` or `nvm -v`\
- `nvm install 14`\

### Service Mocking
Quick File Server\
- install serve package `npm install -g serve`\
- create folder static `node -e "fs.mkdirSync('static')"`\
- unlink server.js `node -e "fs.unlinkSync('server.js')"`\
- use fastify and link folder `node -e "fs.mkdirSync('mock-srv')`\
